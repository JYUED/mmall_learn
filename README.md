# MMall

一个整合SSM框架的企业级电商项目

## 项目来源

项目来源于国内IT公开课平台[慕课网](http://www.imooc.com)。项目分为十二章节

- 架构演进
- 开发环境安装与配置
- 数据库表结构设计
- 项目初始化
- 用户模块开发
- 分类管理模块开发
- 商品管理模块开发
- 购物车模块开发
- 收获地址管理模块开发
- 支付模块开发
- 订单管理模块开发
- 云服务器线上部署自动化发布

## 项目环境的搭建

* **操作系统**：Centos6.8
* **IDE**：IntelliJ IDEA 2015
* **JDK**：JDK1.7
* **Web容器**：Tomcat 7.0.75
* **数据库**：Mysql 5.1.73
* **依赖管理工具**：Maven 3.0.5 

这里列出的环境不是必须的,你喜欢用什么就用什么,这里只是给出参考,不过不同的版本可能会引起各种不同的问题就需要我们自己去发现以及排查,在这里使用Maven的话时方便我们管理JAR包,我们不用跑去各种开源框架的官网去下载一个又一个的JAR包,配置好了Maven后添加pom文件坐标就会从中央仓库下载JAR包,如果哪天替换版本也很方便

Geely提供的环境下载地址及资料
<http://learning.happymmall.com/>

---
## 项目效果
- 前台展示：<http://happymall.shop/> <http://www.happymall.shop/> 
- 后台管理：<http://admin.happymall.shop/>
- 接口文档：<http://git.oschina.net/imooccode/happymmallwiki>

---
## 项目运行
### 下载
`Download Zip`或`git clone`

```shell
git clone https://git.oschina.net/happymmall2017/mmall_learn.git
```
### 导入到IDE
这里因为是使用`IDEA`创建的项目,所以使用IDEA直接打开是很方便的,提前是你要配置好`maven`的相关配置,以及项目`JDK`版本

- IDEA 
直接在主界面选择Open,然后找到项目所在路径,点击导入就可以了

## Let's start!
项目总结可能比较长,**密集恐惧症**请按小节阅读
- [架构演进]()
- [开发环境安装与配置]()
- [数据库表结构设计]()
- [项目初始化]()
- [用户模块开发]()
- [分类管理模块开发]()
- [商品管理模块开发]()
- [购物车模块开发]()
- [收获地址管理模块开发]()
- [支付模块开发]()
- [订单管理模块开发]()
- [云服务器线上部署自动化发布]()

---
### （一）架构演进

```
大型项目架构演进过程及思考的点
http://www.imooc.com/article/17545
```

### （二）开发环境安装与配置
#### Linux软件源配置学习建议

Linux：Centos6.8 64bit
作死人士请选择RedHat,Ubuntu,CentOs7.3等尝试,这里选择CentOs6.8是由于一个重要的原因就是免费,据说其他版本是收费的,这点笔者没有考证。

推荐使用阿里云镜像<http://mirrors.aliyun.com/>
##### 源配置的步骤

```
1、备份
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup

2、安装CentOs 6
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
或者
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo

3、生成缓存
yum makecache
```

##### 推荐的Linux教程
[1、Linux权限管理址基本权限]()
[2、Linux软件安装管理]()
[3、Linux达人养成计划]()
[4、Linux服务管理]()
[5、学习iptables防火墙]()
[6、版本管理工具介绍-Git]()
[7、Tomcat基础]()
[8、maven基础]()
[9、mysql基础]()

#### Jdk安装
##### Linux上的安装步骤

```
rpm -qa | grep jdk 查看jdk

sudo remove XXX （XXX为上一条查询到的结果）删除已有jdk,sudo由权限自己决定是否加

wget http://learning.happymmall.com/jdk/jdk-7u80-linux-x64.rpm
默认路径为：/usr/java/jdk1.7.0_80

sudo vim /etc/profile
在最下面增加
export JAVA_HOME=/usr/java/jdk1.7.0_80
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$JAVA_HOME/bin
:wq保存退出

sudo source /etc/profile
```
ps:
1、官网下载jdk请自行注册oracle账号，现在需要登陆的随机令牌才能下载
2、JAVA配置环境变量的意义:<http://blog.csdn.net/zhouchao001/article/details/50284841>

#### Tomcat安装
##### Linux上的安装步骤

```
wget http://learning.happymmall.com/tomcat/apache-tomcat-7.0.73.tar.gz

tar -zxvf apache-tomcat-7.0.73.tar.gz

sudo vim /etc/profile

export CATALINA_HOME=/developer/apache-tomcat-7.0.73
:wq

sudo source /etc/profile

vim ${CATALINA_HOME}/conf/server.xml

8080端口位置的xml结尾增加URIEncoding="UTF-8"
```
#### Maven安装
##### Maven能干嘛
```
1.用Maven可以方便的创建项目,基于archetype可以创建多种类型的java项目
2.Maven仓库对jar包(artifact)进行统一管理,避免jar文件的重复拷贝和版本冲突
3.团队开发，Maven管理项目的RELEASE和SNAPSHOT版本,方便多模块(Module)项目的各个模块之间的快速集成
```
##### Linux上的安装步骤

```
wget http://learning.happymmall.com/maven/apache-maven-3.0.5-bin.tar.gz

tar -zxvf apache-maven-3.0.5-bin.tar.gz

sudo vim /etc/profile

export MAVEN_HOME=/developer/apache-maven-3.0.5
export PATH=$MAVEN_HOME/bin
:wq

sudo source /etc/profile
```



